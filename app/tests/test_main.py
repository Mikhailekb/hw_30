import asyncio
import json

import pytest
from fastapi.testclient import TestClient

from app.config import delete_test_db, init_test_db, session
from app.main import app
from app.models import RecipeModel

pytestmark = pytest.mark.asyncio

client = TestClient(app)


@pytest.fixture(scope="session", autouse=True)
def test_db():
    asyncio.run(init_test_db())
    yield
    asyncio.run(delete_test_db())


async def test_get_all_recipes():
    recipe1 = RecipeModel(
        name="Recipe 1",
        cooking_time=30,
        description="Recipe 1 description",
    )
    recipe2 = RecipeModel(
        name="Recipe 2",
        cooking_time=45,
        description="Recipe 2 description",
    )
    session.add(recipe1)
    session.add(recipe2)
    await session.commit()

    response = client.get("/all_recipes")

    assert response.status_code == 200
    assert response.json() == [
        {
            "name": "Recipe 2",
            "cookingTime": 45,
            "countViews": 0,
            "id": 2,
        },
        {
            "name": "Recipe 1",
            "cookingTime": 30,
            "countViews": 0,
            "id": 1,
        },
    ]


async def test_get_recipe_detail():
    response = client.get("/get_recipe/1")

    assert response.status_code == 200
    assert response.json() == {
        "name": "Recipe 1",
        "cookingTime": 30,
        "id": 1,
        "description": "Recipe 1 description",
        "ingredients": [],
    }


async def test_get_recipe_detail_not_found():
    response = client.get("/get_recipe/100")
    assert response.status_code == 404


async def test_add_ingredient():
    new_ingredient = {"name": "Ingredient 1"}
    response = client.post("/add_ingredient", content=json.dumps(new_ingredient))
    assert response.status_code == 201
    assert response.json() == {"name": "Ingredient 1", "id": 1}


async def test_add_ingredient_bad_schema():
    new_ingredient = {"title": "Ingredient 2"}
    response = client.post("/add_ingredient", content=json.dumps(new_ingredient))

    assert response.status_code == 422


async def test_add_recipe():
    new_recipe = {
        "name": "Recipe 3",
        "cookingTime": 40,
        "description": "Recipe 3 description",
        "ingredients": [1],
    }

    response = client.post("/add_recipe", content=json.dumps(new_recipe))
    assert response.status_code == 201

    new_recipe["id"] = 3
    new_recipe["ingredients"] = [{"name": "Ingredient 1"}]
    assert response.json() == new_recipe


async def test_add_recipe_bad_schema():
    new_recipe = {
        "name": "Recipe 3",
        "cookingTime": 40,
        "description": "Recipe 3 description",
        "ingredients": ["qwerty"],
    }

    response = client.post("/add_recipe", content=json.dumps(new_recipe))
    assert response.status_code == 422
