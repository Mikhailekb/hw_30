from fastapi import FastAPI, HTTPException
from sqlalchemy import ScalarResult
from starlette import status

from app import crud
from app.config import Base, async_engine, session
from app.models import IngredientModel, RecipeModel
from app.schemas import (
    IngredientInSchema,
    IngredientOutSchema,
    RecipeInSchema,
    RecipeOutSchema,
    RecipeReprSchema,
)

app = FastAPI()


@app.on_event("startup")
async def startup():
    async with async_engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)


@app.on_event("shutdown")
async def shutdown():
    await session.close()
    await async_engine.dispose()


@app.get("/all_recipes", response_model=list[RecipeReprSchema])
async def get_all_recipes() -> ScalarResult[RecipeModel]:
    """Get all recipes. Results sorted by popularity"""
    return await crud.get_all_recipes()


@app.get("/get_recipe/{recipe_id}", response_model=RecipeOutSchema)
async def get_recipe_detail(recipe_id: int) -> RecipeModel:
    """Receive a recipe by id"""
    recipe_obj = await crud.get_recipe(recipe_id)

    if recipe_obj:
        recipe_obj.count_views += 1
        return recipe_obj
    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND, detail="Recipe not found"
    )


@app.post(
    "/add_recipe",
    response_model=RecipeOutSchema,
    status_code=status.HTTP_201_CREATED,
)
async def add_recipe(recipe: RecipeInSchema) -> RecipeModel:
    """Adding a new recipe"""
    try:
        new_recipe = await crud.create_recipe(recipe)
    except ValueError as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=str(e)
        ) from e

    return new_recipe


@app.post(
    "/add_ingredient",
    response_model=IngredientOutSchema,
    status_code=status.HTTP_201_CREATED,
)
async def add_ingredient(ingredient: IngredientInSchema) -> IngredientModel:
    """Adding a new ingredient"""
    try:
        ingredient_db = await crud.create_ingredient(ingredient)
    except ValueError as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=str(e)
        ) from e

    return ingredient_db


if __name__ == "__main__":
    import uvicorn

    uvicorn.run("main:app", host="localhost", port=8000, reload=True)
