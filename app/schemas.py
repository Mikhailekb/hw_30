from pydantic import BaseModel, Field


class IngredientBase(BaseModel):
    name: str = Field(max_length=100, description="Ingredient name")


class IngredientInSchema(IngredientBase):
    pass


class IngredientOutSchema(IngredientBase):
    id: int


class RecipeBase(BaseModel):
    name: str = Field(max_length=100, description="Recipe name")
    cooking_time: int = Field(
        gt=0,
        description="Cooking time in minutes",
        serialization_alias="cookingTime",
    )


class RecipeInSchema(RecipeBase):
    ingredients: list[int] | None = Field(
        description="Expecting an array of ingredient IDs"
    )
    description: str = Field(description="Recipe description")
    cooking_time: int = Field(
        gt=0,
        description="Cooking time in minutes",
        alias="cookingTime",
    )


class RecipeOutSchema(RecipeBase):
    id: int
    ingredients: list[IngredientInSchema] | None = None
    description: str = Field(description="Recipe description")


class RecipeReprSchema(RecipeBase):
    id: int
    count_views: int = Field(
        ge=0,
        description="Number of views for this recipe",
        serialization_alias="countViews",
    )
