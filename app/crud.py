from sqlalchemy import ScalarResult, select
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import joinedload, load_only

from app.config import session
from app.models import IngredientModel, RecipeModel
from app.schemas import IngredientInSchema, RecipeInSchema


async def get_all_recipes() -> ScalarResult[RecipeModel]:
    stmt = (
        select(RecipeModel)
        .options(
            load_only(
                RecipeModel.id,
                RecipeModel.name,
                RecipeModel.count_views,
                RecipeModel.cooking_time,
            ),
        )
        .order_by(-RecipeModel.count_views, -RecipeModel.cooking_time)
    )
    return await session.scalars(stmt)


async def get_recipe(recipe_id: int) -> RecipeModel | None:
    stmt = (
        select(RecipeModel)
        .options(
            joinedload(RecipeModel.ingredients),
        )
        .where(RecipeModel.id == recipe_id)
    )
    return await session.scalar(stmt)


async def create_recipe(recipe: RecipeInSchema) -> RecipeModel:
    data = recipe.model_dump()
    id_ingredients = data.pop("ingredients")
    new_recipe = RecipeModel(**data)

    await new_recipe.add_ingredients(id_ingredients)

    session.add(new_recipe)
    await session.commit()

    return new_recipe


async def create_ingredient(ingredients: IngredientInSchema) -> IngredientModel:
    new_ingredient = IngredientModel(**ingredients.model_dump())
    session.add(new_ingredient)

    try:
        await session.commit()
    except IntegrityError as e:
        await session.rollback()

        raise ValueError("Ingredient already exists") from e

    return new_ingredient
