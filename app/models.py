from sqlalchemy import Column, ForeignKey, String, Table, Text, select
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.config import Base, session

recipe_to_ingredients = Table(
    "recipe_to_ingredients",
    Base.metadata,
    Column("recipe_id", ForeignKey("recipes.id")),
    Column("ingredients_id", ForeignKey("ingredients.id")),
)


class RecipeModel(Base):
    __tablename__ = "recipes"

    name: Mapped[str] = mapped_column(String(100))
    cooking_time: Mapped[int]
    description: Mapped[str] = mapped_column(Text())
    count_views: Mapped[int] = mapped_column(default=0, server_default="0")

    ingredients: Mapped[list["IngredientModel"]] = relationship(
        secondary=lambda: recipe_to_ingredients,
    )

    async def add_ingredients(self, ingredients_id: list[int]):
        not_found_ingredients = []
        for ingredient_id in ingredients_id:
            stmt = select(IngredientModel).where(IngredientModel.id == ingredient_id)
            ingredient = await session.scalar(stmt)
            if ingredient:
                self.ingredients.append(ingredient)
            else:
                not_found_ingredients.append(str(ingredient_id))

        if not_found_ingredients:
            raise ValueError(
                f"Not found ingredients with id {', '.join(not_found_ingredients)}"
            )


class IngredientModel(Base):
    __tablename__ = "ingredients"

    name: Mapped[str] = mapped_column(String(100), unique=True)
