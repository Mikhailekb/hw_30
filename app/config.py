import os
from pathlib import Path

from sqlalchemy.ext.asyncio import async_sessionmaker, create_async_engine
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column

BASE_DIR = Path(__file__).parent

if os.environ.get("TEST"):
    DB_PATH = BASE_DIR / "test_db.sqlite3"
else:
    DB_PATH = BASE_DIR / "db.sqlite3"

async_engine = create_async_engine(f"sqlite+aiosqlite:///{DB_PATH}", echo=False)


class Base(DeclarativeBase):
    __abstract__ = True

    id: Mapped[int] = mapped_column(primary_key=True)


async_session = async_sessionmaker(
    bind=async_engine,
    expire_on_commit=False,
)

session = async_session()


async def init_test_db():
    async with async_engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)


async def delete_test_db():
    await session.close()
    await async_engine.dispose()
    DB_PATH.unlink()
